//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"", "cat /tmp/recordingicon",			 1,			0},
	{"", "~/dwmblocks/statusbar/sb-date",		 1,			0},
	{"", "~/dwmblocks/statusbar/sb-clock",		 1,			0},
	{"", "~/dwmblocks/statusbar/sb-forecast",        1,			0},
	{"", "~/dwmblocks/statusbar/sb-internet",        1,			0},
	{"", "~/dwmblocks/statusbar/sb-volume",	         1,			0},
	{"", "~/dwmblocks/statusbar/sb-battery",	 1,			0},
	{"", "~/dwmblocks/statusbar/sb-price",	         1,			0},
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = "|";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

